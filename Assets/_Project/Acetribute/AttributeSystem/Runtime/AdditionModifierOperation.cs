using System;
using UnityEngine;

namespace SanTeeg0.Acetribute.AttributeSystem.Runtime
{
    [Serializable]
    public struct AdditionModifierOperation : IModifierOperation
    {
        [SerializeField] private float _addValue;

        public float CalculateResult(float sourceValue)
        {
            return sourceValue + _addValue;
        }
    }
}
