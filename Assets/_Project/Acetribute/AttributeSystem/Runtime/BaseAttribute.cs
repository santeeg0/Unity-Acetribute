using System.Collections.Generic;
using UnityEngine;

namespace SanTeeg0.Acetribute.AttributeSystem.Runtime
{
    public abstract class BaseAttribute : ScriptableObject
    {
        public abstract string Name { get; }

        public abstract float CalculateBaseValue(
                AttributeValue sourceValue,
                AttributeValueModifier modifier,
                IReadOnlyDictionary<BaseAttribute, AttributeValue> attributesValues);

        public abstract float CalculateCurrentValue(
                AttributeValue sourceValue,
                IReadOnlyDictionary<BaseAttribute, AttributeValue> attributesValues);
    }
}
