using System;
using System.Collections.Generic;

namespace SanTeeg0.Acetribute.AttributeSystem.Runtime
{
    [Serializable]
    public struct AttributeValue
    {
        public static AttributeValue Zero = new AttributeValue(0f);

        private List<AttributeValueModifier> _modifiers;

        public AttributeValue(float baseValue)
        {
            BaseValue = baseValue;
            CurrentValue = baseValue;
            _modifiers = new List<AttributeValueModifier>();
        }

        public AttributeValue(
                float baseValue,
                List<AttributeValueModifier> modifiers) : this(baseValue)
        {
            _modifiers = modifiers;
        }

        public float BaseValue { get; private set; }
        public float CurrentValue { get; private set; }
        public IReadOnlyList<AttributeValueModifier> Modifiers => _modifiers;

        public void SetBaseValue(float value)
        {
            BaseValue = value;
        }

        public void SetCurrentValue(float value)
        {
            CurrentValue = value;
        }

        public void AddModifier(AttributeValueModifier modifier)
        {
            _modifiers.Add(modifier);
        }

        public void RemoveModifier(AttributeValueModifier modifier)
        {
            _modifiers.Remove(modifier);
        }
    }
}
