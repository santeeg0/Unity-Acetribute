using System;

namespace SanTeeg0.Acetribute.AttributeSystem.Runtime
{
    public delegate void AttributeValueChangedDelegate(
            BaseAttribute attribute,
            AttributeValue oldValue, 
            ref AttributeValue newValue);

    public interface IAttributedCharacter
    {
        public event Action<BaseAttribute> AttributeAdded;
        public event Action<BaseAttribute> AttributeRemoved;
        public event AttributeValueChangedDelegate AttributeValueChanged;

        public bool AddAttribute(BaseAttribute attribute);

        public bool RemoveAttribute(BaseAttribute attribute);

        public bool GetAttributeValue(BaseAttribute attribute, out AttributeValue value);

        public bool ModifyAttributeBaseValue(
                BaseAttribute attribute,
                AttributeValueModifier modifier,
                out AttributeValue newValue);

        public bool ModifyAttributeCurrentValue(
                BaseAttribute attribute,
                AttributeValueModifier modifier,
                out AttributeValue newValue);

        public bool RevertAttributeCurrentValueModification(
                BaseAttribute attribute,
                AttributeValueModifier modifier,
                out AttributeValue newValue);
    }
}
