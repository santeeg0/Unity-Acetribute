using UnityEngine;

namespace SanTeeg0.Acetribute.AttributeSystem.Runtime
{
    public abstract class BaseAttributeEvent : ScriptableObject
    {
        public abstract BaseAttribute TargetAttribute { get; }
        public abstract float Priority { get; }

        public abstract void OnAttributeAdded(IAttributedCharacter character);

        public abstract void OnAttributeRemoved(IAttributedCharacter character);

        public abstract void OnAttributeValueChanged(
                IAttributedCharacter character,
                AttributeValue oldValue,
                ref AttributeValue newValue);
    }
}
