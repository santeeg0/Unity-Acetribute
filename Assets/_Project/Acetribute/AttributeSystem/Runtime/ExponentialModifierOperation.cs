using System;
using UnityEngine;

namespace SanTeeg0.Acetribute.AttributeSystem.Runtime
{
    [Serializable]
    public struct ExponentialModifierOperation : IModifierOperation
    {
        [SerializeField] private float _exponentialValue;

        public float CalculateResult(float sourceValue)
        {
            return Mathf.Pow(sourceValue, _exponentialValue);
        }
    }
}
