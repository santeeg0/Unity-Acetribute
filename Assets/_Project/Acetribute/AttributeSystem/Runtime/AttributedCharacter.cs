using System;
using System.Collections.Generic;
using UnityEngine;

namespace SanTeeg0.Acetribute.AttributeSystem.Runtime
{
    public class AttributedCharacter : MonoBehaviour, IAttributedCharacter
    {
        private Dictionary<BaseAttribute, AttributeValue> _attributesValues;

        public event Action<BaseAttribute> AttributeAdded;
        public event Action<BaseAttribute> AttributeRemoved;
        public event AttributeValueChangedDelegate AttributeValueChanged;

        public bool AddAttribute(BaseAttribute attribute)
        {
            if (HasAttribute(attribute))
            {
                return false;
            }

            _attributesValues.Add(attribute, AttributeValue.Zero); 
            return true;
        }

        public bool RemoveAttribute(BaseAttribute attribute)
        {
            if (!HasAttribute(attribute))
            {
                return false;
            }

            _attributesValues.Remove(attribute);
            return true;
        }

        public bool GetAttributeValue(BaseAttribute attribute, out AttributeValue value)
        {
            if (!HasAttribute(attribute))
            {
                value = AttributeValue.Zero;
                return false;
            }

            value = _attributesValues[attribute];
            return true;
        }

        public bool ModifyAttributeBaseValue(
                BaseAttribute attribute,
                AttributeValueModifier modifier,
                out AttributeValue newValue)
        {
            if (!HasAttribute(attribute))
            {
                newValue = AttributeValue.Zero;
                return false;
            }

            var oldValue = _attributesValues[attribute];
            newValue = oldValue;
            var newBaseValue = attribute.CalculateBaseValue(newValue, modifier, _attributesValues);
            newValue.SetBaseValue(newBaseValue);
            var newCurrentValue = attribute.CalculateCurrentValue(newValue, _attributesValues);
            newValue.SetCurrentValue(newCurrentValue);
            AttributeValueChanged?.Invoke(attribute, oldValue, ref newValue);
            _attributesValues[attribute] = newValue;
            return true;
        }

        public bool ModifyAttributeCurrentValue(
                BaseAttribute attribute,
                AttributeValueModifier modifier,
                out AttributeValue newValue)
        {
            if (!HasAttribute(attribute))
            {
                newValue = AttributeValue.Zero;
                return false;
            }

            var oldValue = _attributesValues[attribute];
            newValue = oldValue;
            newValue.AddModifier(modifier);
            var newCurrentValue = attribute.CalculateCurrentValue(newValue, _attributesValues);
            newValue.SetCurrentValue(newCurrentValue);
            AttributeValueChanged?.Invoke(attribute, oldValue, ref newValue);
            _attributesValues[attribute] = newValue;
            return true;
        }

        public bool RevertAttributeCurrentValueModification(
                BaseAttribute attribute,
                AttributeValueModifier modifier,
                out AttributeValue newValue)
        {
            if (!HasAttribute(attribute))
            {
                newValue = AttributeValue.Zero;
                return false;
            }

            var oldValue = _attributesValues[attribute];
            newValue = oldValue;
            newValue.RemoveModifier(modifier);
            var newCurrentValue = attribute.CalculateCurrentValue(newValue, _attributesValues);
            AttributeValueChanged?.Invoke(attribute, oldValue, ref newValue);
            _attributesValues[attribute] = newValue;
            return true;
        }

        private bool HasAttribute(BaseAttribute attribute)
        {
            return _attributesValues.ContainsKey(attribute);
        }
    }
}
