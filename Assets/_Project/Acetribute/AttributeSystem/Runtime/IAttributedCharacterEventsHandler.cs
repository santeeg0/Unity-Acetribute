namespace SanTeeg0.Acetribute.AttributeSystem.Runtime
{
    public interface IAttributedCharacterEventsHandler
    {
        public void AddAttributeEvent(BaseAttributeEvent attributeEvent);

        public void RemoveAttributeEvent(BaseAttributeEvent attributeEvent);
    }
}
