namespace SanTeeg0.Acetribute.AttributeSystem.Runtime
{
    public interface IModifierOperation
    {
        public float CalculateResult(float sourceValue);
    }
}
