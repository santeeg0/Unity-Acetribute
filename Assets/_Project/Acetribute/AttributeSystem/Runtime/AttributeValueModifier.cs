using System;
using System.Collections.Generic;

namespace SanTeeg0.Acetribute.AttributeSystem.Runtime
{
    [Serializable]
    public struct AttributeValueModifier
    {
        private List<IModifierOperation> _operations;

        public IReadOnlyList<IModifierOperation> Operations => _operations;

        public float CalculateOperationsResult(float sourceValue)
        {
            for (var i = 0; i < _operations.Count; ++i)
            {
                var operation = _operations[i];
                sourceValue = operation.CalculateResult(sourceValue);
            }
            return sourceValue;
        }
    }
}
