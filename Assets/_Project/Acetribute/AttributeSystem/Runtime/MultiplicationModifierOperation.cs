using System;
using UnityEngine;

namespace SanTeeg0.Acetribute.AttributeSystem.Runtime
{
    [Serializable]
    public struct MultiplicationModifierOperation : IModifierOperation
    {
        [SerializeField] private float _multiplyValue;

        public float CalculateResult(float sourceValue)
        {
            return sourceValue * _multiplyValue;
        }
    }
}
