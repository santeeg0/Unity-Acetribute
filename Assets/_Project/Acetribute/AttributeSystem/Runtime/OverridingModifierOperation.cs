using System;
using UnityEngine;

namespace SanTeeg0.Acetribute.AttributeSystem.Runtime
{
    [Serializable]
    public struct OverridingModifierOperation : IModifierOperation
    {
        [SerializeField] private float _overrideValue;

        public float CalculateResult(float sourceValue)
        {
            return _overrideValue;
        }
    }
}
