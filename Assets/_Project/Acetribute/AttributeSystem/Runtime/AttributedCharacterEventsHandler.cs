using System.Collections.Generic;
using UnityEngine;

namespace SanTeeg0.Acetribute.AttributeSystem.Runtime
{
    public class AttributedCharacterEventsHandler : MonoBehaviour, IAttributedCharacterEventsHandler
    {
        private List<BaseAttributeEvent> _attributesEvents;
        private IAttributedCharacter _attributedCharacter;

        public void AddAttributeEvent(BaseAttributeEvent attributeEvent)
        {
            _attributesEvents.Add(attributeEvent);
        }

        public void RemoveAttributeEvent(BaseAttributeEvent attributeEvent)
        {
            _attributesEvents.Remove(attributeEvent);
        }

        private void OnAttributeAdded(BaseAttribute attribute)
        {
            for (var i = 0; i < _attributesEvents.Count; ++i)
            {
                var attributeEvent = _attributesEvents[i];
                if (attribute == attributeEvent.TargetAttribute)
                {
                    attributeEvent.OnAttributeAdded(_attributedCharacter);
                }
            }
        }

        private void OnAttributeRemoved(BaseAttribute attribute)
        {
            for (var i = 0; i < _attributesEvents.Count; ++i)
            {
                var attributeEvent = _attributesEvents[i];
                if (attribute == attributeEvent.TargetAttribute)
                {
                    attributeEvent.OnAttributeRemoved(_attributedCharacter);
                }
            }
        }

        private void OnAttributeValueChanged(
                BaseAttribute attribute,
                AttributeValue oldValue,
                ref AttributeValue newValue)
        {
            for (var i = 0; i < _attributesEvents.Count; ++i)
            {
                var attributeEvent = _attributesEvents[i];
                if (attribute == attributeEvent.TargetAttribute)
                {
                    attributeEvent.OnAttributeValueChanged(
                            _attributedCharacter,
                            oldValue,
                            ref newValue);
                }
            }
        }

        private void Awake()
        {
            _attributedCharacter = GetComponent<IAttributedCharacter>();
        }

        private void OnEnable()
        {
            _attributedCharacter.AttributeAdded += OnAttributeAdded;
            _attributedCharacter.AttributeRemoved += OnAttributeRemoved;
            _attributedCharacter.AttributeValueChanged += OnAttributeValueChanged;
        }

        private void OnDisable()
        {
            _attributedCharacter.AttributeAdded -= OnAttributeAdded;
            _attributedCharacter.AttributeRemoved -= OnAttributeRemoved;
            _attributedCharacter.AttributeValueChanged -= OnAttributeValueChanged;
        }
    }
}
